#ifndef _BSP_DMA2D_H
#define _BSP_DMA2D_H


#include "main.h"


void _DMA2D_Fill(void * pDst, 
								uint32_t xSize, 
								uint32_t ySize, 
								uint32_t OffLine, 
								uint32_t ColorIndex, 
								uint32_t PixelFormat);


void _DMA2D_Copy(void * pSrc, 
								void * pDst, 
								uint32_t xSize, 
								uint32_t ySize, 
								uint32_t OffLineSrc, 
								uint32_t OffLineDst, 
								uint32_t PixelFormat);


void _DMA2D_MixColorsBulk(uint32_t * pColorFG,  
													uint32_t OffLineSrcFG,
													uint32_t * pColorDst, 
													uint32_t OffLineDst,
													uint32_t xSize, 
													uint32_t ySize, 
													uint8_t Intens);

void _DMA2D_AlphaBlendingBulk(uint32_t * pColorFG,  
															uint32_t OffLineSrcFG,
															uint32_t * pColorBG,  
															uint32_t OffLineSrcBG,
															uint32_t * pColorDst, 
															uint32_t OffLineDst,
															uint32_t xSize, 
															uint32_t ySize); 


void _DMA2D_DrawAlphaBitmap(void  * pDst, 
														void  * pSrc, 
														uint32_t xSize, 
														uint32_t ySize, 
														uint32_t OffLineSrc, 
														uint32_t OffLineDst, 
														uint32_t PixelFormat);

														
														
void bsp_dma2d_color_mix_565(void *psrc,void *pdest,uint32_t length,uint8_t opa);														
void bsp_dma2d_color_mix_A888(void *psrc,void *pdest,uint32_t length,uint8_t opa);
														
														
														
#endif

