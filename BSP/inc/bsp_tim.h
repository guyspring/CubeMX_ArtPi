#ifndef _BSP_TIM_H
#define _BSP_TIM_H


#include "main.h"




void bsp_tim_start(void);
void bsp_tim_stop(void);
uint16_t bsp_tim_get_count(void);




#endif

