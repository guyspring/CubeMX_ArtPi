#ifndef _LCD_CONF_H
#define _LCD_CONF_H


#include "main.h"


/************************ SDRAM ***************************/
#define  SDRAM_ADDR_START   0xC0000000
#define  SDRAM_ADDR_SIZE    0x02000000

/************************ LCD ***************************/
#define  LCD_WIDTH          800
#define  LCD_HEIGHT         480
 

#define  LCD_FB1_ADDR       0xC0000000
#define  LCD_GUI_BUFF_ADDR1 0xC00BB800
#define  LCD_GUI_BUFF_ADDR2 0xC0177000

typedef enum
{
  lcf_ARGB4444,		
  lcf_ARGB1555,	
  lcf_RGB565,
  lcf_RGB888,	
  lcf_ARGB888,		
}lcd_color_format;






#endif

